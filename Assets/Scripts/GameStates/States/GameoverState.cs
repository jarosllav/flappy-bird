using UnityEngine;
using Zenject;

public class GameoverState : GameState {
    private StateMachine stateMachine;

    public GameoverState([Inject(Id = "GameStates")]StateMachine stateMachine) {
        this.stateMachine = stateMachine;
    }

    public override void Start() {
        GUIGameover menu = GameObject.FindObjectOfType<GUIGameover>();
        menu?.Show();

        this.pauseParallax();

        GUIGameover.onPlayButton += this.onPlayButton;
    }

    public override void Exit() {
        GUIGameover menu = GameObject.FindObjectOfType<GUIGameover>();
        menu?.Hide();

        this.unpauseParallax();

        GUIGameover.onPlayButton -= this.onPlayButton;
    }

    public override void Update() { }

    private void onPlayButton() {
        this.stateMachine.ChangeState(GameStates.GAMEPLAY);
    }

    private void pauseParallax() {
        Parallax[] parallaxObjects = GameObject.FindObjectsOfType<Parallax>();
        foreach(Parallax parallax in parallaxObjects) {
            parallax.isPaused = true;
        }
    }

    private void unpauseParallax() {
        Parallax[] parallaxObjects = GameObject.FindObjectsOfType<Parallax>();
        foreach(Parallax parallax in parallaxObjects) {
            parallax.isPaused = false;
        }
    }

    public class Factory : PlaceholderFactory<GameoverState> {}
}