using UnityEngine;
using Zenject;

public class MenuState : GameState {
    private StateMachine stateMachine;
    private Player player;

    public MenuState([Inject(Id = "GameStates")]StateMachine stateMachine, Player player) {
        this.stateMachine = stateMachine;
        this.player = player;
    }

    public override void Start() {
        this.player.isUseGravity = false;
        this.player.isHandleInput = false;

        GUIMenu menu = GameObject.FindObjectOfType<GUIMenu>();
        menu?.Show();

        GUIMenu.onPlayButton += this.onPlayButton;
    }

    public override void Exit() {
        GUIMenu menu = GameObject.FindObjectOfType<GUIMenu>();
        menu?.Hide();

        GUIMenu.onPlayButton -= this.onPlayButton;
    }

    public override void Update() { }

    private void onPlayButton() {
        this.stateMachine.ChangeState(GameStates.GAMEPLAY);
    }

    public class Factory : PlaceholderFactory<MenuState> {}
}