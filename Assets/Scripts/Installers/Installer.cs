using UnityEngine;
using Zenject;

public class Installer : MonoInstaller
{
    [Inject] Settings settings;

    public override void InstallBindings() {
        this.installPlayer();
        this.installPipes();
        this.installHelpers();
        this.installGameStates();
    }

    private void installPlayer() {
        Container.BindInterfacesAndSelfTo<BombManager>().AsSingle();
        Container.Bind<Player>()
            .FromComponentInNewPrefab(this.settings.birdPrefab)
            .WithGameObjectName("Player")
            .AsSingle();
    }

    private void installPipes() {
        Container.BindInterfacesAndSelfTo<PipesManager>().AsSingle();
        Container.BindInstance(new ObjectPool<Pipe>()).AsSingle();
        Container.BindFactory<Pipe, Pipe.Factory>()
            .FromComponentInNewPrefab(this.settings.pipePrefab);
    }

    private void installHelpers() {
        Container.Bind<Camera>().WithId("MainCamera").FromInstance(Camera.main);
        Container.Bind<WorldHelper>().AsSingle();
        Container.Bind<ScoreSerializer>().AsSingle();
    }

    private void installGameStates() {
        Container.Bind<StateMachine>().WithId("GameStates").AsSingle();
        Container.Bind<GameStateFactory>().AsSingle();

        Container.BindInterfacesAndSelfTo<MenuState>().AsSingle();
        Container.BindFactory<MenuState, MenuState.Factory>().WhenInjectedInto<GameStateFactory>();

        Container.BindInterfacesAndSelfTo<GameplayState>().AsSingle();
        Container.BindFactory<GameplayState, GameplayState.Factory>().WhenInjectedInto<GameStateFactory>();

        Container.BindInterfacesAndSelfTo<GameoverState>().AsSingle();
        Container.BindFactory<GameoverState, GameoverState.Factory>().WhenInjectedInto<GameStateFactory>();
    }

    [System.Serializable]
    public class Settings {
        public GameObject birdPrefab;
        public GameObject pipePrefab;
    }
}