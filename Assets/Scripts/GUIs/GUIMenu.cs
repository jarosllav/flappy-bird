using System;
using UnityEngine;

public class GUIMenu : MonoBehaviour
{
    public static event Action onPlayButton;

    public void OnPlayButton() {
        GUIMenu.onPlayButton?.Invoke();
    }

    [ContextMenu("Show")]
    public void Show() {
        for(int i = 0; i < transform.childCount; ++i)
            transform.GetChild(i).gameObject.SetActive(true);
    }
    
    [ContextMenu("Hide")]
    public void Hide() {
        for(int i = 0; i < transform.childCount; ++i)
            transform.GetChild(i).gameObject.SetActive(false);
    }
}
