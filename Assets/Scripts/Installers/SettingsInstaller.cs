using UnityEngine;
using Zenject;

//[CreateAssetMenu(fileName = "SettingsInstaller", menuName = "Installers/SettingsInstaller")]
public class SettingsInstaller : ScriptableObjectInstaller<SettingsInstaller>
{
    public Installer.Settings gameSettings;
    public Player.Settings playerSettings;
    public PipesManager.Settings pipesSettings;

    public override void InstallBindings() {
        Container.BindInstance(this.gameSettings);
        Container.BindInstance(this.playerSettings);
        Container.BindInstance(this.pipesSettings);
    }
}