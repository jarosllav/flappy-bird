using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class ScoreSerializer {
    private const int maxScores = 5;
    private string dataPath;
    private SortedSet<int> scores;

    public ScoreSerializer() {
        this.dataPath = Application.persistentDataPath + "/scores.dat";
        this.scores = new SortedSet<int>();
    }

    public void LoadScores() {
        if(!File.Exists(this.dataPath))
            return;

        BinaryFormatter formatter = new BinaryFormatter();

        FileStream file = new FileStream(this.dataPath, FileMode.Open, FileAccess.Read, FileShare.None);

        this.scores = (SortedSet<int>)formatter.Deserialize(file);
        file.Close();
    }

    public void SaveScores() {
        BinaryFormatter formatter = new BinaryFormatter();

        FileStream file = new FileStream(this.dataPath, FileMode.Create, FileAccess.Write, FileShare.None);

        formatter.Serialize(file, this.scores);
        file.Close();
    }

    public void InsertScore(int score) {
        this.scores.Add(score);

        if(this.scores.Count > maxScores)
            this.scores.Remove(this.scores.First());

        this.SaveScores();
    }

    public int GetHighestScore() {
        return this.scores.Count > 0 ? this.scores.Last() : 0;
    }
}