using UnityEngine;
using Zenject;

public class GameManager : MonoBehaviour
{
    private StateMachine stateMachine;
    private ScoreSerializer scoreSerializer;
    private PipesManager pipesManager;
    private Player player;

    [Inject]
    public void Construct([Inject(Id = "GameStates")]StateMachine stateMachine, ScoreSerializer scoreSerializer, Player player, PipesManager pipesManager) {
        this.stateMachine = stateMachine;
        this.scoreSerializer = scoreSerializer;
        this.player = player;
        this.pipesManager = pipesManager;
    }

    private void Start() {
        this.scoreSerializer.LoadScores();
        this.stateMachine.ChangeState(GameStates.MENU);
    }

    private void Update() {
        this.stateMachine.Update();
    }

    private void FixedUpdate() {
        this.stateMachine.FixedUpdate();
    }

    public ScoreSerializer GetScoreSerializer() {
        return this.scoreSerializer;
    }
}