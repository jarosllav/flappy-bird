using System;
using System.Collections.Generic;

public interface IPoolable {
    void Prepare();
    void Dispose();
}

public class ObjectPool<T> where T : IPoolable
{
    public Func<T> factory;
    private Queue<T> pool;

    public ObjectPool() {
        this.pool = new Queue<T>();
    }

    public T Get() {
        if(this.pool.Count > 0) {
            T poolable = this.pool.Dequeue();
            poolable.Prepare();
            return poolable;
        }
        else {
            T poolable = this.factory();
            return poolable;
        }
    }

    public void Release(T poolable) {
        poolable.Dispose();
        this.pool.Enqueue(poolable);
    }
}
