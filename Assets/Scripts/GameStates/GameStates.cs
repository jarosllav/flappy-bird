public enum GameStates {
    MENU,
    GAMEPLAY,
    PAUSE,
    GAMEOVER
}