using System;

public class BombManager {
    public static event Action<int> onChangeBombsAmount;
    public static event Action onUseBomb;

    public int maxBombs;
    
    private PipesManager pipesManager;
    private int bombs;

    public BombManager(PipesManager pipesManager) {
        this.pipesManager = pipesManager;
    }

    public bool UseBomb() {
        if(this.bombs > 0) {
            this.SetBombsAmount(this.bombs - 1);
            this.pipesManager.ClearPipes();
            BombManager.onUseBomb?.Invoke();
            return true;
        }
        return false;
    }

    public void GiveBomb() {
        if(this.bombs < this.maxBombs) {
            this.SetBombsAmount(this.bombs + 1);
        }
    }

    public void SetBombsAmount(int amount) {
        this.bombs = amount;
        BombManager.onChangeBombsAmount?.Invoke(this.bombs);
    }
}