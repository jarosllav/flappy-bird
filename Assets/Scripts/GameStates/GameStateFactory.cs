public class GameStateFactory {
    private MenuState.Factory menuStateFactory;
    private GameplayState.Factory gameplayStateFactory;
    private GameoverState.Factory gameoverStateFactory;

    public GameStateFactory(MenuState.Factory menuStateFactory, GameplayState.Factory gameplayStateFactory, GameoverState.Factory gameoverStateFactory) {
        this.menuStateFactory = menuStateFactory;
        this.gameplayStateFactory = gameplayStateFactory;
        this.gameoverStateFactory = gameoverStateFactory;
    }

    public GameState Create(GameStates state) {
        switch(state) {
            case GameStates.MENU:
                return this.menuStateFactory.Create();
            case GameStates.GAMEPLAY:
                return this.gameplayStateFactory.Create();
            case GameStates.GAMEOVER:
                return this.gameoverStateFactory.Create();
        }

        return null;
    }
}