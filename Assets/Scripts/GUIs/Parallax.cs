using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    public bool isPaused = false;
    public float speed = 1f;

    private MeshRenderer meshRenderer;

    private void Awake() {
        this.meshRenderer = GetComponent<MeshRenderer>();
    }

    private void Update() {
        if(!this.isPaused)
            this.meshRenderer.material.mainTextureOffset += Vector2.right * this.speed * Time.deltaTime;
    }
}
