public class GameState
{
    public virtual void Start() { }
    public virtual void Exit() { }
    public virtual void Update() { }
}