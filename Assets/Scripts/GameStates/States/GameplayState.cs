using UnityEngine;
using Zenject;

public class GameplayState : GameState {
    private StateMachine stateMachine;
    private Player player;
    private PipesManager pipesManager;

    public GameplayState([Inject(Id = "GameStates")]StateMachine stateMachine, Player player, PipesManager pipesManager) {
        this.stateMachine = stateMachine;
        this.player = player;
        this.pipesManager = pipesManager;
    }

    public override void Start() {
        this.player.Reset();
        this.pipesManager.ClearPipes();

        GUIGameplay menu = GameObject.FindObjectOfType<GUIGameplay>();
        menu?.Show();

        Player.onHitPipe += this.onPlayerHitPipe;
    }

    public override void Exit() {
        GUIGameplay menu = GameObject.FindObjectOfType<GUIGameplay>();
        menu?.Hide();

        Player.onHitPipe -= this.onPlayerHitPipe;
    }

    public override void Update() {
        this.pipesManager.Update();
    }

    private void onPlayerHitPipe() {
        this.player.isHandleInput = false;
        this.player.isUseGravity = false;

        this.stateMachine.ChangeState(GameStates.GAMEOVER);
    }

    public class Factory : PlaceholderFactory<GameplayState> {}
}