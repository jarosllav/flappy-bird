using System;
using UnityEngine;
using Zenject;

public class Player : MonoBehaviour {
    public static event Action onHitPipe;
    public static event Action<int> onChangeScore;

    public bool isHandleInput = false;
    public bool isUseGravity = false;
    public float velocity = 0f;
    
    private Settings settings;
    private BombManager bombManager;
    private AudioSource audioSource;
    private float doubleTapTime;
    private int tapCount = 0;
    private int score = 0;

    [Inject]
    public void Contruct(Settings settings, BombManager bombManager) {
        this.settings = settings;
        this.bombManager = bombManager;
    }

    private void Awake() {
        this.bombManager.maxBombs = this.settings.maxBombs;
        this.audioSource = GetComponent<AudioSource>();
    }

    private void Update() {
        if(this.isHandleInput)
            this.handleInput();
    }    

    private void FixedUpdate() {
        if(this.isUseGravity)
            this.updateGravity();
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.tag == "Pipe") {
            Player.onHitPipe?.Invoke();
        }
        else if(other.tag == "Score") {
            this.SetScore(this.score + 1);

            if(this.score % 10 == 0) {
                this.bombManager.GiveBomb();
            }
        }
    }

    public void Reset() {
        this.transform.position = Vector3.zero;
        this.isHandleInput = true;
        this.isUseGravity = true;
        this.velocity = 0f;
        this.SetScore(0);
        this.bombManager.SetBombsAmount(0);
    }

    public void SetScore(int score) {
        this.score = score;
        Player.onChangeScore?.Invoke(this.score);
    }

    public int GetScore() {
        return this.score;
    }
    
    private void handleInput() {
        #if UNITY_EDITOR
        if(Input.GetButtonDown("Jump")) {
            this.jump();
        }
        #endif

        if(Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began) {
            ++this.tapCount;
            this.jump();
        }

        if(this.tapCount > 0) {
            this.doubleTapTime += Time.deltaTime;
        }

        if(this.tapCount >= 2) {
            this.doubleTapTime = 0f;
            this.tapCount = 0;

            this.useBomb();
        }

        if(this.doubleTapTime > this.settings.doubleTapSpeed) {
            this.doubleTapTime = 0f;
            this.tapCount = 0;
        }
    }

    private void jump() {
        this.velocity = this.settings.jumpForce;
        this.audioSource.PlayOneShot(this.settings.jumpSound);
    }

    private void useBomb() {
        if(this.bombManager.UseBomb())
            this.audioSource.PlayOneShot(this.settings.explosionSound);
    }

    private void updateGravity() {
        this.velocity += this.settings.gravity * Time.fixedDeltaTime;
        
        Vector3 position = transform.position;
        position.y += this.velocity * Time.fixedDeltaTime;
        transform.position = position;

        this.updateRotation();
    }

    private void updateRotation() {
        float zRotation = 10f * this.velocity;
        zRotation = Mathf.Clamp(zRotation, -40, 30);
        
        transform.localRotation = Quaternion.Euler(0f, 0f, zRotation);
    }

    [System.Serializable]
    public class Settings {
        public float gravity = -9.81f;
        public float jumpForce = 5f;
        public float doubleTapSpeed = 0.3f;
        public int maxBombs = 3;
        public AudioClip jumpSound;
        public AudioClip explosionSound;
    }
}