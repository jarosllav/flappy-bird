using System;
using UnityEngine;
using Zenject;

public class GUIGameover : MonoBehaviour
{
    public static event Action onPlayButton;

    [SerializeField] private TMPro.TextMeshProUGUI scoreText;
    [SerializeField] private TMPro.TextMeshProUGUI highscoreText;
    private ScoreSerializer scoreSerializer;
    private Player player;

    [Inject]
    public void Construct(ScoreSerializer scoreSerializer, Player player) {
        this.scoreSerializer = scoreSerializer;
        this.player = player;
    }

    [ContextMenu("Show")]
    public void Show() {
        for(int i = 0; i < transform.childCount; ++i)
            transform.GetChild(i).gameObject.SetActive(true);

        int score = this.player.GetScore();

        this.updateScoreText(score);
        this.scoreSerializer.InsertScore(score);

        if(this.checkScore(score))
            this.highscoreText?.gameObject.SetActive(false);

        GetComponent<AudioSource>()?.Play();
    }
    
    [ContextMenu("Hide")]
    public void Hide() {
        for(int i = 0; i < transform.childCount; ++i)
            transform.GetChild(i).gameObject.SetActive(false);
    }

    public void OnPlayButton() {
        GUIGameover.onPlayButton?.Invoke();
    }

    private bool checkScore(int score) {
        return score <= this.scoreSerializer.GetHighestScore();
    }

    private void updateScoreText(int score) {
        this.scoreText?.SetText(score.ToString());
    }
}
