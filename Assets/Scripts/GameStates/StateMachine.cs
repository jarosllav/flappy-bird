using UnityEngine.Assertions;

public class StateMachine {
    private GameStateFactory gameStateFactory;
    private GameState gameState;

    public StateMachine(GameStateFactory gameStateFactory) {
        this.gameStateFactory = gameStateFactory;
    }

    public void Update() {
        this.gameState?.Update();
    }

    public void FixedUpdate() {
        //this.gameState?.FixedUpdate();
    }

    public GameState GetActiveState() {
        return this.gameState;
    }

    public void ChangeState(GameStates state) {
        this.gameState?.Exit();

        this.gameState = this.gameStateFactory.Create(state);
        Assert.IsNotNull(this.gameState);

        this.gameState.Start();
    }
}