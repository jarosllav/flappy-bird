using UnityEngine;
using Zenject;

public class WorldHelper {
    private Camera camera;

    public WorldHelper([Inject(Id = "MainCamera")] Camera camera) {
        this.camera = camera;
    }

    public float bottom {
        get { return -height; }
    }

    public float top {
        get { return height; }
    }

    public float left {
        get { return -width; }
    }

    public float right {
        get { return width; }
    }

    public float height {
        get { return this.camera.orthographicSize; }
    }

    public float width {
        get { return this.camera.aspect * this.camera.orthographicSize; }
    }
}