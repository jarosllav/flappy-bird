using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIGameplay : MonoBehaviour
{
    [SerializeField] private TMPro.TextMeshProUGUI scoreText;
    [SerializeField] private TMPro.TextMeshProUGUI bombText;

    private void Awake() {
        Player.onChangeScore += this.onChangeScore;
        BombManager.onChangeBombsAmount += this.onChangeBombs;
    }

    [ContextMenu("Show")]
    public void Show() {
        for(int i = 0; i < transform.childCount; ++i)
            transform.GetChild(i).gameObject.SetActive(true);
    }
    
    [ContextMenu("Hide")]
    public void Hide() {
        for(int i = 0; i < transform.childCount; ++i)
            transform.GetChild(i).gameObject.SetActive(false);
    }

    private void onChangeScore(int currentScore) {
        this.scoreText?.SetText(currentScore.ToString());
    }   

    private void onChangeBombs(int bombsAmount) {
        this.bombText?.SetText(bombsAmount.ToString());

        if(bombsAmount > 0) //To prevent play sound at start ;/
            GetComponent<AudioSource>()?.Play();
    }   
}
