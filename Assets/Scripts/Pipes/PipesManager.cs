using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class PipesManager {
    private Settings settings;
    private WorldHelper worldHelper;
    private ObjectPool<Pipe> pipesPool;

    private List<Pipe> pipes;
    private float spawnTimer;

    public PipesManager(Settings settings, WorldHelper worldHelper, ObjectPool<Pipe> pipesPool, Pipe.Factory factory) {
        this.settings = settings;
        this.worldHelper = worldHelper;
        this.pipesPool = pipesPool;
        this.pipesPool.factory = factory.Create;

        this.pipes = new List<Pipe>();
        this.spawnTimer = this.settings.density;
    }

    public void SpawnPipe() {
        float y = UnityEngine.Random.Range(this.settings.minY, this.settings.maxY);
        Pipe pipe = pipesPool.Get();
        
        Vector3 position = new Vector3(worldHelper.right + pipe.halfWidth, y, 0f);
        pipe.transform.position = position;

        this.pipes.Add(pipe);
    }

    public void ClearPipes() {
        this.spawnTimer = this.settings.density;
        for(int i = this.pipes.Count - 1; i >= 0; --i) {
            this.removePipeAt(i);
        }
    }

    public void Update() {
        this.updatePipesMovement();
        this.updateSpawnTimer();
    }

    private void updatePipesMovement() {
        for(int i = this.pipes.Count - 1; i >= 0; --i) {
            Pipe pipe = this.pipes[i];

            pipe.transform.Translate(Vector3.left * this.settings.scrollSpeed * Time.deltaTime);
            if(pipe.transform.position.x < this.worldHelper.left - pipe.halfWidth) {
                this.removePipeAt(i);
            }
        }
    }

    private void updateSpawnTimer() {
        this.spawnTimer -= Time.deltaTime;
        if(spawnTimer <= 0f) {
            SpawnPipe();
            this.spawnTimer = this.settings.density;
        }
    }

    private void removePipeAt(int id) {
        this.pipesPool.Release(this.pipes[id]);
        this.pipes.RemoveAt(id);
    }

    [System.Serializable]
    public class Settings {
        public float minY = -2.5f;
        public float maxY = 2.5f;
        public float density = 1.5f;
        public float scrollSpeed = 1f;
    }
}