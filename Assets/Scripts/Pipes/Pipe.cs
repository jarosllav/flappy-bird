using UnityEngine;
using Zenject;

public class Pipe : MonoBehaviour, IPoolable
{
    private GameObject part1;

    public float width {
        get { return this.part1.GetComponent<SpriteRenderer>().bounds.size.x; }
    }
    public float halfWidth {
        get { return this.part1.GetComponent<SpriteRenderer>().bounds.size.x / 2f; }
    }

    private void Awake() {
        this.part1 = transform.GetChild(0).gameObject;
    }

    void IPoolable.Dispose() {
        gameObject.SetActive(false);
    }

    void IPoolable.Prepare() {
        gameObject.SetActive(true);
    }

    public class Factory : PlaceholderFactory<Pipe> {}
}